using UnityEngine;

using System.Collections;

 

public class Lift : MonoBehaviour {

	public GUISkin liftSkin;
	static public bool onLift = false;
	private int currentFloorLift9;
	private int currentFloorLift7;
	private bool havePicked = false;
	private bool Lift9 = false;
	private bool Lift7 = false;
	
	
	
    void Start ()
	{
		currentFloorLift9 = 1;
		currentFloorLift7 = 1;
	}

    // Update:
    void Update () 
	{

    }  
	
	IEnumerator delayTime()
	{
		 yield return new WaitForSeconds(2.0f);
	}
	
	void  OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == "Player")
		{
			Debug.Log("Entered the onTriggerEnter");
			if(this.gameObject.tag == "LiftRoom9")
			{
				Debug.Log(other);
				Cursor.visible = true;
				delayTime();
				onLift = true;
				Lift9 = true;
    			other.gameObject.transform.parent=gameObject.transform;
			}
			
			if(this.gameObject.tag == "LiftRoom7")
			{
				Debug.Log(other);
				Cursor.visible = true;
				delayTime();
				onLift = true;
				Lift7 = true;
    			other.gameObject.transform.parent=gameObject.transform;
			}
		}
		
 	}
 
	void OnTriggerExit(Collider other)
	{
		if(other.gameObject.tag == "Player")
		{
			other.gameObject.transform.parent = null; 
			onLift = false;
			havePicked = false;
			Lift9 = false;
			Lift7 = false;
		}
	}
	
	void OnGUI()
	{
		GUI.skin = liftSkin;
		if(onLift && !havePicked)
		{
			if(Lift9)
			{
				GUI.Box (new Rect ((Screen.width-200)/2,80,260,140), "");
				if(currentFloorLift9 == 0)
				{
					GUI.Label(new Rect((Screen.width-200)/2,80,200,180),"Would you like to go to \n\n     floor 1 or 2");
					if (GUI.Button (new Rect ((Screen.width-160)/2,180,110,25), "Floor 1")) 
					{
						GetComponent<Animation>().Play("BasToFl1");
						currentFloorLift9 = 1;
						havePicked = true;
					}
					if (GUI.Button (new Rect ((Screen.width + 60)/2,180,110,25), "Floor 2")) 
					{
						GetComponent<Animation>().Play("BasToFl2");
						currentFloorLift9 = 2;
						havePicked = true;
					}
				}
				if(currentFloorLift9 == 1)
				{
					GUI.Label(new Rect((Screen.width-200)/2,80,300,180),"Would you like to go to the \n\n     basment or floor 2");
					if (GUI.Button (new Rect ((Screen.width-160)/2,180,110,25), "Basement")) 
					{
						GetComponent<Animation>().Play("Fl1ToBas");
						currentFloorLift9 = 0;
						havePicked = true;
					}
					if (GUI.Button (new Rect ((Screen.width + 60)/2,180,110,25), "Floor 2")) 
					{
						GetComponent<Animation>().Play("Fl1ToFl2");
						currentFloorLift9 = 2;
						havePicked = true;
					}
				}
				if(currentFloorLift9 == 2)
				{
					GUI.Label(new Rect((Screen.width-200)/2,80,200,180),"Would you like to go to the \n\n     basement or floor 1");
					if (GUI.Button (new Rect ((Screen.width-160)/2,180,110,25), "Basement")) 
					{
						GetComponent<Animation>().Play("Fl2ToBas");
						currentFloorLift9 = 0;
						havePicked = true;
					}
					if (GUI.Button (new Rect ((Screen.width + 60)/2,180,110,25), "Floor 1")) 
					{
						GetComponent<Animation>().Play("Fl2ToFl1");
						currentFloorLift9 = 1;
						havePicked = true;
					}
				}
			}
			
			if(Lift7)
			{
				GUI.Box (new Rect ((Screen.width-200)/2,80,260,140), "");
				if(currentFloorLift7 == 0)
				{
					GUI.Label(new Rect((Screen.width-200)/2,80,200,180),"Would you like to go to \n\n     floor 1 or 2");
					if (GUI.Button (new Rect ((Screen.width-160)/2,180,110,25), "Floor 1")) 
					{
						GetComponent<Animation>().Play("BasToFl1Lift7");
						currentFloorLift7 = 1;
						havePicked = true;
					}
					if (GUI.Button (new Rect ((Screen.width + 60)/2,180,110,25), "Floor 2")) 
					{
						GetComponent<Animation>().Play("BasToFl2Lift7");
						currentFloorLift7 = 2;
						havePicked = true;
					}
				}
				if(currentFloorLift7 == 1)
				{
					GUI.Label(new Rect((Screen.width-200)/2,80,300,180),"Would you like to go to the \n\n     basment or floor 2");
					if (GUI.Button (new Rect ((Screen.width-160)/2,180,110,25), "Basement")) 
					{
						GetComponent<Animation>().Play("Fl1ToBasLift7");
						currentFloorLift7 = 0;
						havePicked = true;
					}
					if (GUI.Button (new Rect ((Screen.width + 60)/2,180,110,25), "Floor 2")) 
					{
						GetComponent<Animation>().Play("Fl1ToFl2Lift7");
						currentFloorLift7 = 2;
						havePicked = true;
					}
				}
				if(currentFloorLift7 == 2)
				{
					GUI.Label(new Rect((Screen.width-200)/2,80,200,180),"Would you like to go to the \n\n     basement or floor 1");
					if (GUI.Button (new Rect ((Screen.width-160)/2,180,110,25), "Basement")) 
					{
						GetComponent<Animation>().Play("Fl2ToBasLift7");
						currentFloorLift7 = 0;
						havePicked = true;
					}
					if (GUI.Button (new Rect ((Screen.width + 60)/2,180,110,25), "Floor 1")) 
					{
						GetComponent<Animation>().Play("Fl2ToFl1Lift7");
						currentFloorLift7 = 1;
						havePicked = true;
					}
				}
			}
		}
	}

}