﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {
	public int choice = 1;
	public Texture2D Movement;
	public Texture2D LeftButton;
	public Texture2D RightButton;
	
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI()
    {
		GUIStyle boxStyle = "box";
		boxStyle.wordWrap = true;
		if(choice == 1)
		{
			if (GUI.Button(new Rect(32, 70, 130, 30), "Start the Game!"))
	        {
	            SceneManager.LoadScene("GameLevel");	
	        }
			if(GUI.Button(new Rect(32,110,130,30),"Instructions"))
			{
				choice = 3;	
			}
			if(GUI.Button(new Rect(32,150,130,30),"Quit Game"))
			{
				Application.Quit();	
			}
		}
		if(choice == 3)
		{
			GUI.Box(new Rect(32, 200, 100, 100),"The player moves around forward back left and right using these keys.",boxStyle);
			GUI.Box(new Rect(32, 300, 100, 100),Movement);
			
			GUI.Box(new Rect(232,200,100,100),"Allows the player to attack with spells and with the current weapon equiped",boxStyle);
			GUI.Box(new Rect(232, 300, 100, 100),LeftButton);
			
			GUI.Box(new Rect(432,200,100,100),"Allows the player to raise and lower his shield",boxStyle);
			GUI.Box(new Rect(432, 300, 100, 100),RightButton);
			if (GUI.Button(new Rect(32, 400, 100, 30), "Back"))
        	{
				choice = 1;
        	}	
		}
	}
}