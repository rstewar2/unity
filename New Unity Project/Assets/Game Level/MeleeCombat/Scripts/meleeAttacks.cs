using UnityEngine;
using System.Collections;

public class meleeAttacks : MonoBehaviour
{	
	public GameObject sword;
	public GameObject shield;
	public AnimationClip swordSwing1;
	public AnimationClip shieldRaise;
	public AnimationClip shieldLower;
	public AnimationClip shieldBash;
	
	public static bool shieldRaised;
	
	// Use this for initialization
	void Start () {
	shieldRaised = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetMouseButton(0))
		{
			if (!sword.GetComponent<Animation>().isPlaying && !shieldRaised)
			{
				sword.GetComponent<Animation>().clip = swordSwing1;
				sword.GetComponent<Animation>().Play();
			}
			
			else if (!shield.GetComponent<Animation>().isPlaying && shieldRaised)
			{
				shield.GetComponent<Animation>().clip = shieldBash;
				shield.GetComponent<Animation>().Play();
			}
		}
		
		if (Input.GetMouseButtonDown(1))
		{
			if (!shield.GetComponent<Animation>().isPlaying && !shieldRaised)
			{
				shieldRaised = true;
				shield.GetComponent<Animation>().clip = shieldRaise;
				shield.GetComponent<Animation>().Play ();
				PlayerMove.moveSpeed /= 2;
			}
			
			else if (!shield.GetComponent<Animation>().isPlaying && shieldRaised)
			{
				shieldRaised = false;
				shield.GetComponent<Animation>().clip = shieldLower;
				shield.GetComponent<Animation>().Play();
				PlayerMove.moveSpeed *= 2;
			}
		}
	}
}