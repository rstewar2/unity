using UnityEngine;
using System.Collections;

public class Monster : MonoBehaviour {
	
	public string name;
	public int attack;
	public int defense;
	public int hitPoints;
	public int expGiven;
	public int goldGiven;
	public int itemsDropped;
	
	public Monster(string pName,int pAttack,int pDefense,int pHitPoints,int pExpGiven,int pGoldGiven,int pItemsDropped)
	{
		name = pName;
	    attack = pAttack;
		defense = pDefense;
		hitPoints = pHitPoints;
		expGiven = pExpGiven;
		goldGiven = pGoldGiven;
		itemsDropped = pItemsDropped;
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
