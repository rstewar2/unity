using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {
	public static float moveSpeed = 50f;
	CharacterController cc;
	// Use this for initialization
	void Start () {
	
		cc = transform.GetComponent<CharacterController>();
		
	}
	
	// Update is called once per frame
	void Update () {
		
		Vector3 movement = Vector3.zero;
		
				
		if(Input.GetKey(KeyCode.W))
		{
			movement += transform.forward * moveSpeed * Time.deltaTime;
		}
		
		if(Input.GetKey(KeyCode.A))
		{
			movement -= transform.right * moveSpeed * Time.deltaTime;
		}
		
		if(Input.GetKey(KeyCode.D))
		{
			movement += transform.right * moveSpeed * Time.deltaTime;
		}
		
		if(Input.GetKey(KeyCode.S))
		{
			movement -= transform.forward * moveSpeed * Time.deltaTime;
		}
		
		movement.y = cc.velocity.y;
		if(!Lift.onLift)
		movement.y = Physics.gravity.y * Time.deltaTime * 10;
		
		if(cc.isGrounded || Lift.onLift) movement.y = 0;
		//can use -= with own number
		
		
		cc.Move(movement);
	
	}
}
