using UnityEngine;
using System.Collections;

public class itemScript : MonoBehaviour {
	GameObject player;
	GameObject model;
	Vector3 distanceFromPlayer;
	float distanceFromPlayerFloat;
	public float attackDamage;
	public float attackSpeed;
	public float weight;
	public float armorRating;
	public float magicResist;
	public string slot;
	public string name;
	public Texture iconTexture;
	
	
	
	public bool isInRange = false;
	
	public itemScript()
	{
		attackDamage = 0;
		attackSpeed = 0;
		weight = 0;
		armorRating = 0;
		magicResist = 0;
		slot = null;
		name = null;
		iconTexture = null; 
		model = null;
	
	}
	itemScript(float atkDamage, float atkSpeed, float wght, float armrRating, float mgcResist,  string slt, string nam, Texture inputIconTexture, GameObject inputModel)
	{
		attackDamage = atkDamage;
		attackSpeed = atkSpeed;
		weight = wght;
		armorRating = armrRating;
		magicResist = mgcResist;
		slot = slt;
		name = nam;
		iconTexture = inputIconTexture;
		model = inputModel;
	}
	// Use this for initialization
	void Start () 
	{
		
		player = GameObject.Find("player");
		model = this.gameObject;
	}
	
	// Update is called once per frame
	void Update () 
	{
		distanceFromPlayer = player.transform.position - transform.position;
		distanceFromPlayerFloat = distanceFromPlayer.magnitude;
	
		if(distanceFromPlayerFloat < 10)
		{
			isInRange = true;
		}
		else
		{
			isInRange = false;
		}

		if(distanceFromPlayerFloat < 10 && Input.GetKeyDown(KeyCode.Space))
		{
			Menu.playerInventory.addListItem(this);
			
			
			
			
		}
	}
	/*
	public static bool getIsInRange(GameObject thisItem)
	{
		return thisItem.isInRange;
	}
	*/
	void OnGUI()
	{
		if(isInRange)
		{
			GUI.Box(new Rect(Screen.width / 2, Screen.height / 2, 100, 50), "Press Space to \npick up item");
		}
		else
		{
		}
	}
}
