using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;


public class Menu : MonoBehaviour {
	private Vector2 scrollViewVector = Vector2.zero;
	private Texture textureWeapon, textureShield, textureHelmet,texturePants;
	private Texture	textureShoes, textureAccessory1,textureAccessory2;
	public static Texture[] backpackItems;
	public Texture2D health, bGround, experience;
	bool subMenuActive = false;
	bool MenuActive = false;
	bool inventoryArmor = false;
	bool inventoryWeapon = false;
	bool inventoryAccessory = false;
	private int menuChoice = 0;
	private int inventoryChoice;
	public static Character player1;
	public static exampleInventoryClass playerInventory;
	public static exampleInventoryClass itemsWorn;
	string element;
	int tempStrength;
	int tempCharisma ;
	int tempIntel ;
	int tempDex ;
	int tempVigor;
	int tempPoints;
	
	public static char [] inputKeys;
	String[] tempArray;
	
	
	// Use this for initialization
	void Start () {
		backpackItems = new Texture[60];
		
		Cursor.visible = false;
		player1 = new Character();
		playerInventory = new exampleInventoryClass(60);
		itemsWorn = new exampleInventoryClass(2);
		inputKeys = new char[30];
		for(int i = 0; i <= 29; i++)
		{
			inputKeys[i] = 'l';
		}
	}
	
	// Update is called once per frame
	void Update () {
		
		if(player1.currentHP <= 0)
		{
			SceneManager.LoadScene("loss");
			Cursor.visible = true;
		}
		if(player1.currentHP < player1.maxHP)
		{
			player1.currentHP += (double)(player1.maxHP/300)*Time.deltaTime;
		}
		
		player1.ailments();
		
		if (Input.GetKeyDown(KeyCode.Tab) && MenuActive == false) 
		{ 
			player1.experience += 200;
			subMenuActive=!subMenuActive; 
			if(Cursor.visible == true)
			Cursor.visible = false;
			if(Cursor.visible == false)
			Cursor.visible = true;
        }
		searchForLvling();
	}
	
	void OnGUI()
    {
		DrawHUD();
		if(subMenuActive == true)
		{
			// Make a background box 
        	GUI.Box (new Rect ((Screen.width-200)/2,80,200,180), "Menu Screen"); 
        	//First button returns to the game in progress
        	if (GUI.Button (new Rect ((Screen.width-150)/2,110,150,20), "Inventory")) 
			{ 
				subMenuActive=!subMenuActive;
				menuChoice = 1;
				MenuActive=!MenuActive;
            }
			if (GUI.Button (new Rect ((Screen.width-150)/2,130,150,20), "Skills")) 
			{ 
            	subMenuActive=!subMenuActive;
				menuChoice = 2;
				MenuActive=!MenuActive;
            }
			if (GUI.Button (new Rect ((Screen.width-150)/2,150,150,20), "Stats")) 
			{ 
            	subMenuActive=!subMenuActive;
				menuChoice = 3;
				MenuActive=!MenuActive;
            }
        	 //Second button returns to main menu
         	if (GUI.Button (new Rect((Screen.width-150)/2,170,150,20), "Resume Game"))
			{ 
      			subMenuActive=!subMenuActive;
				Cursor.visible = false;
            } 
			if (GUI.Button (new Rect((Screen.width-150)/2,190,150,20), "Main Menu"))
			{ 
      			SceneManager.LoadScene("Main Menu"); 
            } 
			if (GUI.Button (new Rect((Screen.width-150)/2,210,150,20), "Quit Game"))
			{ 
      			Application.Quit(); 
            }
		
		}
		
		
		switch(menuChoice)
		{
			case 1:
			{
				inventory();
				break;	
			}
			case 2:
			{
				Skills();
				break;
			}
			case 3:
			{
				Stats();
				break;
			}
			case 4:
			{
				lvlUp();
				break;
			}
		}
	}
	void inventory()
	{
		if(MenuActive == true)
		{
			GUI.BeginGroup(new Rect(0,0, Screen.width,Screen.height));
			GUI.Box (new Rect (10,10,785,585), "Inventory");
			GUI.Label (new Rect (180, 20, 100, 20), "Helmet");
			GUI.Box( new Rect (150,40,100,190),textureHelmet);
			GUI.Label (new Rect (280, 150, 100, 20), "Weapon");
			GUI.Box( new Rect (280,170,100,130),textureWeapon);
			GUI.Label (new Rect (40, 150, 100, 20), "Shield");
			GUI.Box( new Rect (20,170,100,130),textureShield);
			GUI.Label (new Rect (180, 230, 100, 20), "Pants");
			GUI.Box( new Rect (150,250,100,150),texturePants);
			GUI.Label (new Rect (180, 400, 100, 20), "Shoes");
			GUI.Box( new Rect (150,420,100,130),textureShoes);
			GUI.Label (new Rect (280, 300, 100, 20), "Accesory");
			GUI.Box( new Rect (280,320,100,130),textureAccessory1);
			GUI.Label (new Rect (40, 300, 100, 20), "Accesory");
			GUI.Box( new Rect (20,320,100,130),textureAccessory2);
			
			int l = 0;
			for(int i = 0; i < playerInventory.InventoryList.Length / 6; i++)
			{
				for(int j = 0; j < playerInventory.InventoryList.Length / 10; j++)
					{	
						if(GUI.Button (new Rect(450 + (50* j), 40 + (50 * i), 50, 50), playerInventory.InventoryList[l].iconTexture) && playerInventory.InventoryList[l].iconTexture != null) 
						{
							Debug.Log ("clicked an item");
							
							if(playerInventory.InventoryList[l].slot == "Weapon")
							{
								if(textureWeapon == null)
								{
									textureWeapon = playerInventory.InventoryList[l].iconTexture;							
								//	itemsEquipped.itemChange(playerInventory.InventoryList[l]);
								//	playerInventory.removeListItem(playerInventory.InventoryList[l]);
									
								}
								else if(textureWeapon != null)
								{
									textureWeapon = playerInventory.InventoryList[l].iconTexture;								
								//	itemsEquipped.itemChange(playerInventory.InventoryList[l]);
								//	playerInventory.removeListItem(playerInventory.InventoryList[l]);
									
								}
							}
							else if(playerInventory.InventoryList[l].slot == "Shield")
							{
								if(textureShield == null)
								{
									textureShield = playerInventory.InventoryList[l].iconTexture;								
								//	itemsEquipped.itemChange(playerInventory.InventoryList[l]);
								//	playerInventory.removeListItem(playerInventory.InventoryList[l]);
									
								}
								else if(textureShield != null)
								{
									textureShield = playerInventory.InventoryList[l].iconTexture;
								//	itemsEquipped.itemChange(playerInventory.InventoryList[l]);
								//	playerInventory.removeListItem(playerInventory.InventoryList[l]);
									
								}
							}
						}	
							l++;
					}

			}
	
			if (GUI.Button (new Rect (690,570,100,20), "Back"))
			{
				subMenuActive=!subMenuActive;
				MenuActive=!MenuActive;
				menuChoice = 0;
			}
			
			GUI.EndGroup();
		}		
	}
	void Skills()
	{
		GUI.BeginGroup(new Rect(0,0, Screen.width,Screen.height));
		//string data;
		GUI.Box (new Rect(25,25, Screen.width-50, Screen.height -50), "Passive");
		GUI.Label(new Rect(225, 25, 90, 20), "Active");
		scrollViewVector = GUI.BeginScrollView (new Rect (40, 50, 450, 650), scrollViewVector, new Rect (0, 0, 0, 800));
		GUI.Box (new Rect (0,0,450,800), "");		
		
		if (GUI.Button (new Rect (40,710,150,20), "Inventory")) 
		{ 
			menuChoice = 1;
        }
		if (GUI.Button (new Rect (190,710,150,20), "Stats")) 
		{ 
           	menuChoice = 3;
        }
         //Second button returns to main menu
         if (GUI.Button (new Rect(530,710,150,20), "Resume Game"))
		{ 
      		menuChoice =0; 
			MenuActive=!MenuActive;
			Cursor.visible = false;
        } 
		if (GUI.Button (new Rect(680,710,150,20), "Main Menu"))
		{ 
      		SceneManager.LoadScene("Main Menu"); 
        } 
		if (GUI.Button (new Rect(830,710,150,20), "Quit Game"))
		{ 
      		Application.Quit(); 
        } 
		
		GUI.EndGroup();
	}
	void Stats()
	{	
		GUI.Box (new Rect(25,25, Screen.width-50, Screen.height -50), "Stats");
		if(player1.availablePoints > 0)
		{
			if(GUI.Button(new Rect ((Screen.width-200)/2,Screen.height-100,200,25), "Assign Points"))
			{
				tempPoints = player1.availablePoints;
				tempStrength = player1.strength;
				tempDex = player1.dexterity;
				tempIntel = player1.intelligence;
				tempVigor = player1.vigor;
				tempCharisma = player1.charisma;
				menuChoice = 4;
			}
		}
		
		GUI.Label( new Rect(40, 50, 100, 20), "level " + player1.level);
		GUI.Label( new Rect(40, 90, 100, 30), "HP " + (int)player1.currentHP + "/" + player1.maxHP);
		
		GUI.Label(new Rect(40, 150, 100, 30), "Strength " + player1.strength);
		GUI.Label(new Rect(40, 170, 100, 30), "Charisma " + player1.charisma);
		GUI.Label(new Rect(40, 190, 100, 30), "Intelligence " + player1.intelligence);
		GUI.Label(new Rect(40, 210, 100, 30), "Dexterity " + player1.dexterity);
		GUI.Label(new Rect(40, 230, 100, 30), "Vigor " + player1.vigor);
		
		GUI.Label( new Rect(Screen.width/2 - 100, 50, 200, 20), "Experience");
		GUI.Label( new Rect(Screen.width/2 - 100, 70, 200, 20), "" + player1.experience);
		
		GUI.Label( new Rect(Screen.width/2 - 100, 90, 200, 20), "Experience till level up");
		GUI.Label( new Rect(Screen.width/2 - 100, 110, 200, 20), "" + (player1.nextLvlXP-player1.experience));
		
		GUI.Label( new Rect(Screen.width/2 - 100, 130, 200, 20), "Available Points");
		GUI.Label( new Rect(Screen.width/2 - 100, 150, 200, 20), "" + player1.availablePoints);
		
		if (GUI.Button (new Rect ((Screen.width-180),110,150,20), "Inventory")) 
		{ 
           
			menuChoice = 1;
        }
		if (GUI.Button (new Rect ((Screen.width-180),130,150,20), "Skills")) 
		{ 
        	
			menuChoice = 2;
        }
         //Second button returns to main menu
         if (GUI.Button (new Rect ((Screen.width-180),150,150,20), "Resume Game")) 
		{ 
      		menuChoice =0; 
			MenuActive=!MenuActive;
			Cursor.visible = false;
        } 
		if (GUI.Button (new Rect((Screen.width-180),170,150,20), "Main Menu"))
		{ 
      		SceneManager.LoadScene("Main Menu"); 
        } 
		if (GUI.Button (new Rect((Screen.width-180),190,150,20), "Quit Game"))
		{ 
      		Application.Quit(); 
        } 
	}
	
	void lvlUp()
	{
		player1.maxHP = 100 * tempVigor;
		GUI.Box(new Rect(25,25, Screen.width-50, Screen.height -50), "Leveling Up");
		
		GUI.Label( new Rect(40, 50, 100, 20), "level " + player1.level);
		GUI.Label( new Rect(40, 70, 200, 20), "Element type: " + element);
		GUI.Label( new Rect(40, 90, 100, 30), "HP " + (int)player1.currentHP + "/" + player1.maxHP);
		
		GUI.Label(new Rect(40, 150, 100, 30), "Strength " + tempStrength);
		GUI.Label(new Rect(40, 170, 100, 30), "Charisma " + tempCharisma);
		GUI.Label(new Rect(40, 190, 100, 30), "Intelligence " + tempIntel);
		GUI.Label(new Rect(40, 210, 100, 30), "Dexterity " + tempDex);
		GUI.Label(new Rect(40, 230, 100, 30), "Vigor " + tempVigor);
		
		GUI.Label( new Rect(Screen.width/2 - 100, 50, 200, 20), "Experience");
		GUI.Label( new Rect(Screen.width/2 - 100, 70, 200, 20), "" + player1.experience);
		
		GUI.Label( new Rect(Screen.width/2 - 100, 90, 200, 20), "Experience till level up");
		GUI.Label( new Rect(Screen.width/2 - 100, 110, 200, 20), "" + (player1.nextLvlXP-player1.experience));
		
		GUI.Label( new Rect(Screen.width/2 - 100, 130, 200, 20), "Available Points");
		GUI.Label( new Rect(Screen.width/2 - 100, 150, 200, 20), "" + tempPoints);
		
		if(GUI.Button(new Rect(140, 150, 20,20),"+"))
		{
			if(tempPoints > 0)
			{
				tempStrength++;
				tempPoints--;
			}
		}
		if(GUI.Button(new Rect(165, 150, 20,20),"-"))
		{
			if(tempStrength > player1.strength)
			{
				tempStrength--;
				tempPoints++;
			}
		}
		if(GUI.Button(new Rect(140, 170, 20,20),"+"))
		{
			if(tempPoints > 0)
			{
				tempCharisma++;
				tempPoints--;
			}
		}
		if(GUI.Button(new Rect(165, 170, 20,20),"-"))
		{
			if(tempCharisma > player1.charisma)
			{
				tempCharisma--;
				tempPoints++;
			}
		}
		if(GUI.Button(new Rect(140, 190, 20,20),"+"))
		{
			if(tempPoints > 0)
			{
				tempIntel++;
				tempPoints--;
			}
		}
		if(GUI.Button(new Rect(165, 190, 20,20),"-"))
		{
			if(tempIntel > player1.intelligence)
			{
				tempIntel--;
				tempPoints++;
			}
		}
		if(GUI.Button(new Rect(140, 210, 20,20),"+"))
		{
			if(tempPoints > 0)
			{
				tempDex++;
				tempPoints--;
			}
		}
		if(GUI.Button(new Rect(165, 210, 20,20),"-"))
		{
			if(tempDex > player1.dexterity)
			{
				tempDex--;
				tempPoints++;
			}
		}
		if(GUI.Button(new Rect(140, 230, 20,20),"+"))
		{
			if(tempPoints > 0)
			{
				tempVigor++;
				tempPoints--;
			}
		}
		if(GUI.Button(new Rect(165, 230, 20,20),"-"))
		{
			if(tempVigor > player1.vigor)
			{
				tempVigor--;
				tempPoints++;
			}
		}
		if(GUI.Button(new Rect(Screen.width / 2 - 100, Screen.height-200, 200,100),"Accept Changes"))
		{
			PlayerMove.moveSpeed += (tempDex - player1.dexterity) * 2;
			player1.strength = tempStrength;
			player1.dexterity = tempDex;
			player1.intelligence = tempIntel;
			player1.vigor = tempVigor;
			player1.charisma = tempCharisma;
			
			if(tempPoints % 3 == 0 && tempPoints != player1.availablePoints)
			{
				player1.lvlUp();
			}
			
			player1.availablePoints = tempPoints;
			player1.currentHP = player1.maxHP;
			menuChoice = 3;
		}
		if(GUI.Button(new Rect(Screen.width / 2 - 100, Screen.height-90, 200,50),"Undo"))
		{
			tempPoints = player1.availablePoints;
			tempStrength = player1.strength;
			tempDex = player1.dexterity;
			tempIntel = player1.intelligence;
			tempVigor = player1.vigor;
			tempCharisma = player1.charisma;
		}
		if(GUI.Button(new Rect(Screen.width / 2 + 105, Screen.height-200, 200,100),"Exit with no changes"))
		{
			tempPoints = player1.availablePoints;
			tempStrength = player1.strength;
			tempDex = player1.dexterity;
			tempIntel = player1.intelligence;
			tempVigor = player1.vigor;
			tempCharisma = player1.charisma;
			player1.maxHP = player1.vigor * 100;
			menuChoice = 3;
		}
	}
	
	void searchForLvling()
	{
		if(player1.experience >= player1.nextLvlXP)
		{
			player1.availablePoints += 3;
			player1.nextLvlXP *= 2;
			player1.level++;
		}
	}
	
	void DrawHUD()
	{
		if(!subMenuActive && menuChoice == 0)
		{
			GUI.Label(new Rect(20, 42, 200, 20), "" + (int)player1.currentHP + "/" + player1.maxHP);
			GUI.Label(new Rect(Screen.width - 120, 42, 100, 20), player1.nextLvlXP-player1.experience + " experience needed for next level");
			GUI.Label(new Rect(Screen.width - 120, 62, 100, 20), "Level " + player1.level);
		
			if(Event.current.type.Equals(EventType.Repaint))
			{
				Graphics.DrawTexture(new Rect(20, 20, 200,20), bGround);
				Graphics.DrawTexture(new Rect(22, 22, ((float)player1.currentHP/(float)player1.maxHP)* 196, 16), health);
				Graphics.DrawTexture(new Rect(Screen.width - 120, 20, 100,20), bGround);
				if(player1.level <= 1)
				{
					Graphics.DrawTexture(new Rect(Screen.width - 118, 22, ((float)player1.experience/(float)player1.nextLvlXP)* 96, 16), experience);
				}
				else
				{
					Graphics.DrawTexture(new Rect(Screen.width - 118, 22, ((float)(player1.experience - (player1.nextLvlXP/2))/(float)(player1.nextLvlXP/2))* 96, 16), experience);
				}
			}
		
		}

	}
}


