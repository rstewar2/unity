using UnityEngine;
using System.Collections;

public class Character
{
	public int level;
	public int maxHP;
	
	public int strength;
	public int intelligence;
	public int charisma;
	public int dexterity;
	public int vigor;
	
	private int armor;
	public double currentHP;
	
	public int experience;
	public int nextLvlXP;
	public int availablePoints;
	
	public bool [] status;
	
	
	public Character()
	{
		level = 1;
		maxHP = 500;
		currentHP = 500;
		
		strength = 5;
		intelligence = 5;
		charisma = 5;
		dexterity = 5;
		vigor = 5;
		
		experience = 0;
		nextLvlXP = 500;
		availablePoints = 0;
		status = new bool [10];
		for( int i = 0; i<status.Length; i++)
		{
			status[i] = false;
		}
		
	}
	
	public void lvlUp()
	{
		abilityLvlUp();
		
	}
	
	private void abilityLvlUp()
	{
		
	}
	
	public void ailments()
	{
		//poison
		if(status[0])
		{
			currentHP -= maxHP/150 * Time.deltaTime;
		}
		
		//slow
		if(status[1])
		{
			PlayerMove.moveSpeed /= 2;
		}
		
		//haste
		if(status[2])
		{
			PlayerMove.moveSpeed *= 2;
		}
		
		//regen
		if(status[3])
		{
			if(currentHP < maxHP)
			{
				currentHP += maxHP/100 * Time.deltaTime;
			}
		}
		
		//stun
		if(status[4])
		{
			
		}
	}
}
