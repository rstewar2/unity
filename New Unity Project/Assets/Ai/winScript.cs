using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class winScript : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	void OnGUI()
	{
		GUI.Box(new Rect(Screen.height / 3, Screen.width / 5, Screen.width / 2, Screen.height / 2), "You are victorious!");
		if(GUI.Button(new Rect(Screen.width / 3 , Screen.width / 3, 100, 50), "Play Again"))
		{
			SceneManager.LoadScene("Main Menu");
		}
		if(GUI.Button(new Rect(Screen.width / 2 , Screen.width / 3, 100, 50), "Quit"))
		{
			Application.Quit();
		}
		
	}
}
